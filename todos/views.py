from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list": todo_list}
    return render(request, "todos/list.html", context)


def show_list(request, id):
    show_list = get_object_or_404(TodoList, id=id)
    context = {"show_list": show_list}
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoListForm()

    context = {
        "create_form": form,
    }
    return render(request, "todos/create_list.html", context)


def update_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=list)
    context = {"list": list, "update_form": form}
    return render(request, "todos/update_list.html", context)


def delete_list(request, id):
    list_instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete_list.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "create_item": form,
    }
    return render(request, "todos/create_item.html", context)


def update_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            x = form.save()
            return redirect("todo_list_detail", id=x.list.id)
    else:
        form = TodoItemForm(instance=item)
    context = {"item": item, "update_form": form}
    return render(request, "todos/update_item.html", context)
